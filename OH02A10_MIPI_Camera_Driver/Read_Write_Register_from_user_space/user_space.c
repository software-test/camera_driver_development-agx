
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include<sys/ioctl.h>
 
#define GAIN _IOW('a','a',int16_t*)
#define EXP _IOW('a','b',int16_t*)
#define WR_VALUE _IOW('a','c',int32_t*)
#define RD_VALUE _IOWR('a','d',int32_t*)
int main()
{
        int fd;
        int arr[2];
        volatile int16_t number;
        printf("*********************************\n");
        printf("*******Varun*******\n");
 	int mod;
        printf("\nOpening Driver\n");
        fd = open("/dev/v_device", O_RDWR);
        if(fd < 0) {
                printf("Cannot open device file...\n");
                return 0;
        }

 while(1){
	printf("Enter the mod to send\n");
	printf("0- exit \n1 - gain \n2 - exposure\n3 - Write Register\n4 - Read Register\n");
	printf("Enter the mod to send\n");
	mod=0;
        scanf("%d",&mod);
	if(mod==0)
		break;
	if(mod==3)
		goto write;
        printf("Enter the Value to send\n");
	number=0;
        scanf("%hu",&number);
	printf("Num is %hu\n",number);
write:
	switch(mod){
	case 1: 
		printf("Writing Value to Driver\n");
		ioctl(fd, GAIN, (volatile int16_t*) &number); 
		break;
	case 2: 
		printf("Writing Value to Driver\n");
		ioctl(fd, EXP, (volatile int16_t*) &number); 
		break;
	case 3: 
		printf("enter the addr\n");
		scanf("%d",&arr[0]);
		printf("enter the data\n");
		scanf("%d",&arr[1]);
		ioctl(fd, WR_VALUE, (volatile int16_t*) &arr); 
		break;
	case 4: 
		printf("Writing Value to Driver\n");
		ioctl(fd, RD_VALUE, (volatile int16_t*) &number); 
		printf("The value is 0X%x\n",number);
		break;
	default: 
		break;
	}
 
}
        printf("Closing Driver\n");
        close(fd);
}
